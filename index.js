const numberArr = [];
function nhapSo() {
  numberValue = document.getElementById("txt-number").value * 1;
  numberArr.push(numberValue);
  document.getElementById("txt-number").value = "";
  document.getElementById("ket-qua").innerText = numberArr;
}

// Bài 1
function tinhTong() {
  var tinhTong = 0;
  for (var i = 0; i < numberArr.length; i++) {
    if (numberArr[i] >= 0) {
      tinhTong += numberArr[i];
    } else {
      tinhTong = "Dãy số nhập có số nguyên âm";
    }
  }
  document.getElementById("tinh-tong").innerText = tinhTong;
}

//Bài 2
function demSo() {
  var tongSoDuong = 0;
  for (var i = 0; i < numberArr.length; i++) {
    if (numberArr[i] > 0) {
      tongSoDuong = numberArr.length;
    } else {
      tongSoDuong = "Dãy số nhập có số nguyên âm";
    }
  }
  document.getElementById("dem-so").innerText = tongSoDuong;
}

//Bai 3
function soNhoNhat() {
  var layNhoNhat = Math.min(...numberArr);
  document.getElementById("so-nho-nhat").innerText = layNhoNhat;
}

//Bài 4
function numberIntMin() {
  var soDuongNhoNhat = 0;
  for (var i = 0; i < numberArr.length; i++) {
    if (numberArr[i] > 0) {
      soDuongNhoNhat = Math.min(...numberArr);
    } else {
      soDuongNhoNhat = "Dãy số nhập có số nguyên âm";
    }
  }
  document.getElementById("number-int-min").innerText = soDuongNhoNhat;
}

//Bài 5
function soChan() {
  var soChanCuoi = 0;
  for (var i = 0; i < numberArr.length; i++) {
    if (numberArr[i] % 2 == 0) {
      soChanCuoi = numberArr.lastIndexOf(numberArr[i], numberArr.length);
    } else {
      soChanCuoi = -1;
    }
  }
  document.getElementById("so-chan").innerText = soChanCuoi;
}

//Bài 6

function doiSo() {
  var viTri1 = document.getElementById("txt-vi-tri-1").value * 1;
  var viTri2 = document.getElementById("txt-vi-tri-2").value * 1;
  var giaTriViTri1 = numberArr[viTri1];
  var doiSoViTri1 = numberArr.splice(viTri1, 1, numberArr[viTri2]);
  var doiSo = numberArr.splice(viTri2, 1, giaTriViTri1);
  console.log(numberArr);
  document.getElementById("doi-so").innerText = numberArr;
}

//Bài 7

function sapXep() {
  numberArr.sort(function (a, b) {
    return a - b;
  });

  document.getElementById("sap-xep").innerText = numberArr;
}

//Bài 8
function soNguyenTo(number) {
  // var kiemTra = "";
  var flag = true;

  for (var i = 0; i < number; i++) {
    if (number < 2) {
      flag = false;
    } else {
      for (var n = 2; n <= number; n++) {
        if (number % n == 0) {
          flag = false;
        }
      }
    }
  }

  // if ((flag = true)) {
  //   kiemTra = numberArr[i] + " là số nguyên tố";
  // } else {
  //   kiemTra = "Dãy số này không có số nguyên tố";
  // }

  // document.getElementById("txt-so-nguyen-to").innerText = kiemTra;
}

// function soNguyenTo(value) {
//   if (value < 2) {
//     return false;
//   }
//   if (value >= 2) {
//     for (var i = 1; i <= Math.sqrt(value); i++) {
//       if (value % i == 0) {
//         return false;
//       }
//     }
//     return true;
//   }
// }

function timNguyenTo() {
  for (var i = 0; i <= numberArr.length; i++) {
    if (soNguyenTo(numberArr[i] === true)) {
      console.log(numberArr[i]);
      break;
    } else {
      console.log(-1);
      break;
    }
  }
}

//Bài 9
function timSoNguyen(value) {
  var total = []; 
  for (var i = 0; i < numberArr.length; i++) {
    var soNguyen = Number.isInteger(numberArr[i]);
    if (soNguyen == true) {
      total.push(numberArr[i]);
      document.getElementById("txt-so-nguyen").innerText = total.length;
    } 
  }
}

//Bài 10 
function soSanhHaiSo(n) {
  var flag = 0;
  if (n > 0) {
    flag = 1; 
  } else if (n < 0) {
    flag = -1;
  }
  return flag
}

function soSanh () {
  var soDuong = [];
  var soAm = [];
  var ketQua = "";
  for (var i = 0; i < numberArr.length; i++) {
    if (soSanhHaiSo(numberArr[i]) === 1) {
       soDuong.push(numberArr[i]);
    } else if (soSanhHaiSo(numberArr[i]) === -1) {
      soAm.push(numberArr[i]);
    }
  }
  console.log(soDuong);
  console.log(soAm);
  if (soDuong.length > soAm.length) {
    ketQua = "Tổng số lượng dương lớn hơn tổng số lượng âm";
  } else if (soDuong.length < soAm.length) {
    ketQua = "Tổng số lượng dương nhỏ hơn tổng lượng số âm";
  } else {
    ketQua  = "Tổng số lượng dương bằng tổng số lượng âm"
  }

  document.getElementById("txt-so-sanh").innerText = ketQua;
}
